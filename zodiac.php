<!DOCTYPE html>
<html>
<head>
	<title>Zodiac Sign</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/pulse/bootstrap.css">
</head>
<body class="bg-primary">
	<h1 class="text-light text-center my-5 h1">Welcome Po!</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-4" action="controllers/process.php" method="POST">
			<div class="form-group">
				<label for="firstName">First Name</label>
				<input type="text" name="firstName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month</label>
				<input type="number" name="birthMonth" class="form-control" min="1" max="12">
			</div>
			<div class="form-group">
				<label for="birthDate">Birth Date</label>
				<input type="number" name="birthDate" class="form-control" min="1" max="31">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">Check</button>
			</div>
			<?php 
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
				?>	
			 <p class="text-danger"><?php echo $_SESSION['errorMsg'] ?></p>
			 <?php
				}

			 ?>
		</form>
	</div>

</body>
</html>